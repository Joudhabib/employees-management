const express = require('express');
const mongoose= require('mongoose');
const router = express.Router();
const{ User,validUser}=require('../model/user');
const _ = require('lodash');
const auth=require('../middleWare/auth')
const bcrypt= require('bcryptjs');


router.get('/profile',auth,async(req,res)=>{
const profile=await User.findById(req.user._id).select('-Passward');
res.send(profile);
});
router.post('/',async(req,res)=>{

    const {error} = validUser(req.body)
    if(error){
        console.log(res.send(error.details[0].message));
    }
    let user =await User.findOne({Email:req.body.Email})
    if(user){
        return res.status(404).send('user is found in database');
    }
 user = new User(_.pick(req.body,['fullName','Email','Passward']));
 
const saltRounds=10;
const salt =await bcrypt.genSalt(saltRounds)
user.Passward = await bcrypt.hash(user.Passward,salt);

    await user.save();
    const token=user.generateTokens()
    res.header('x-auth-token',token).send(_.pick(user,['_id','fullName','Email']));
    });
    module.exports=router;