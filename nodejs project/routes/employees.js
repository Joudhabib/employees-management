const express = require('express');
require('express-async-errors');
const router = express.Router();
const{ Employee,validEmployee,validEmployeeforputting}=require('../model/employee');
const auth=require('../middleWare/auth')
const admin=require('../middleWare/admin')
router.get('/' ,(req,res)=>{
    res.send('joud')
    });

    router.get('/employees',async(req,res)=>{
        const employees = await Employee.find().sort('name');
        res.send(employees);
        });
        router.get('/employees/pages',async(req,res)=>{
            const {page=1,limit=10}=req.query
            const employees = await Employee.find().sort('fullName').limit(limit*1).skip((page-1)*limit).exec();
            res.send(employees);
            });
        router.get('/employee/:id',async(req,res)=>{
            try{
    const findEmploye= await Employee.findById(req.params.id)
    res.send(findEmploye);
            }catch(e){
                res.send('not found');
            }
    });
    router.post('/employees',auth,async(req,res )=>{
     
        const {error} = validEmployee(req.body)
        if(error){
            return res.send(error.details[0].message)
        }
    
      
    const employee = new Employee({
      
    fullName: req.body.fullName,
    salary: req.body.salary
    });

    await employee.save();
    res.send(employee);
    });
    router.put('/employee/:id',async(req,res)=>{
     
        const {error} = validEmployeeforputting(req.body)
        if(error){
            return res.send(error.details[0].message)
        }
        const employee = await Employee.findByIdAndUpdate(req.params.id,
            {
            fullName : req.body.fullName
           
        },{new:true})
        if(!employee){
            return res.status(404).send("invalid Id")
        }
       res.send(employee);
    });
     
    router.delete('/employee/:id',[auth,admin],async(req,res)=>{
  
    const employee= await Employee.findByIdAndRemove(req.params.id)

    res.send(employee);
    
    });

    module.exports=router;