const jwt= require('jsonwebtoken');

module.exports=function  (req,res,next){
const token =req.header('x-auth-token');
if(!token){
    return res.status(401).send('acces rejected');
}

try{
const decoder =jwt.verify(token,'privateKey')
req.user=decoder;
next()
}catch(e){
    return res.status(404).send('error...');
}
}