const express= require('express');
const logging= require('./logger/logging')
const employees = require('./routes/employees');
const logger = require('./config/logger');
const users = require('./routes/user');
const auth = require('./routes/Auth');
const hemet=require('helmet');
const morgan= require('morgan');
const mongoose= require('mongoose');
const app = express();
const _ = require('lodash');
const compression=require('compression');
mongoose.connect('mongodb://localhost/Company')
.then(()=>console.log('connected'))
.catch((error)=>logger.log('Error'+error));

const Joi= require('joi');
app.use(express.json());
app.use('/api',employees)
app.use('/api/users',users)
app.use('/api/auth',auth)

app.use(hemet());
app.use(compression());
app.all('*',(req,res,next)=>{
    res.status(404).json({
        status:'false',
        message:'page not found'
    })
})
if(app.get('env')==='development'){
    app.use(logging);
   
    app.use(morgan('tiny'));
}

const port = process.env.port || 3000;
app.listen(port,logger.info('app working'+port+'...'));