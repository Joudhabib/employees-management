
const mongoose= require('mongoose');
const Joi= require('joi');


const Employee =mongoose.model('Employee',new mongoose.Schema({
    fullName:{
        type:String,
        required:true,
        minlength:3,
        maxlength:44,
    },
    salary:{
        type:Number,
        required:true
    }
}))
function validEmployeeforputting(employee){
    const schema = Joi.object({  
        fullName: Joi.string().min(3).required()
         });
  return schema.validate (employee);
        }
    // console.log(joiError)
    function validEmployee(employee){
        const schema = Joi.object({  
            //empId : Joi.number().integer().required(),
            fullName: Joi.string().min(3).required(),
            salary: Joi.number().required()
             });
      return schema.validate (employee);
       
}
exports.Employee = Employee;
exports.validEmployeeforputting=validEmployeeforputting;
exports.validEmployee=validEmployee;