
const mongoose= require('mongoose');
const Joi= require('joi');
const jwt= require('jsonwebtoken');
const userShema=new mongoose.Schema({
    fullName:{
        type:String,
        required:true,
        minlength:3,
        maxlength:44,
    },
    Email:{
        type:String,
        required:true,
        unique:true,
        minlength:3,
        maxlength:255,
    },
    Passward:{
       type:String,
        required:true
   },
   isAdmin:Boolean
  
})


userShema.methods.generateTokens =function(){

    const token=jwt.sign({_id:this._id,isAdmin:this.isAdmin},'privateKey')
    return token;
}
const User =mongoose.model('User',userShema)
    function validUser(user){
        const schema = Joi.object({  
            fullName: Joi.string().min(3).required(),
            Email: Joi.string().min(3).max(255).required().email(),
            Passward: Joi.string().required()
             });
      return schema.validate (user);
            }
        exports.User=User;
        exports.validUser=validUser;